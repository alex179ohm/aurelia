use crate::provider::Provider;

pub struct Endpoint<P: Provider> {
    token: String,
    provider: P,
}

pub struct EndpointBuilder<P: Provider> {
    token: Option<String>,
    provider: P,
}

impl<P: Provider> EndpointBuilder<P> {
    pub fn tocken(self, token: String) -> Self {
        Self {
            token: Some(token),
            provider: self.provider,
        }
    }
    pub fn build(self) -> Endpoint<P> {
        Endpoint {
            token: self.token.unwrap_or_default(),
            provider: self.provider,
        }
    }
}
